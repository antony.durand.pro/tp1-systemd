# TP1-systemd

I. systemd-basics

1. First steps

**Vérification de la version de systemd**

```
systemctl --version


systemd 243 (v243.4-1.fc31)
+PAM +AUDIT +SELINUX +IMA -APPARMOR +SMACK +SYSVINIT +UTMP +LIBCRYPTSETUP 
+GCRYPT +GNUTLS +ACL +XZ +LZ4 +SECCOMP +BLKID +ELFUTILS +KMOD +IDN2 -IDN +PCRE2
default-hierarchy=unified
```


**🌞 s'assurer que systemd est PID1**

pidof systemd
994 1

Cela affiche 2 pid, lequel est le bon, car la commande pidof affiche les pid 
parents et enfants. 

```
pgrep -f systemd
1
650
667
867
994
```


pgrep affiche 

 ```
ps -e | grep systemd
    1 ?        00:00:00 systemd
    650 ?        00:00:00 systemd-journal
    667 ?        00:00:00 systemd-udevd
    867 ?        00:00:00 systemd-logind
    994 ?        00:00:00 systemd
```



ps -e va afficher les processus suivit de leur pid

```
 ps -eF --sort -ppid | grep systemd
user        1073    1001  0 53996   892   3 12:25 pts/0    
root         650       1  0 15189 21760   2 11:59 ?        
root         667       1  0 11475 10816   0 11:59 ?        
root         867       1  0  6491  8416   1 11:59 ? 
```
       


```
user         994        1  0  4893 10256   0 12:02 ?        

root           1        0  0 42205 12416   5 11:58 ?        
```


Nous pouvons donc voir que le processus systemd parent à bien le PID 1 tandis 
que le processus systemd pour l'utilisateur est 994.


**🌞 check tous les autres processus système (NOT kernel processes)**

décrire brièvement tous les autres processus système

```
 ps --ppid 2 -p 2 --deselect
    PID TTY          TIME CMD
      1 ?        00:00:01 systemd
    650 ?        00:00:00 systemd-journal
    667 ?        00:00:00 systemd-udevd
    773 ?        00:00:00 auditd
    775 ?        00:00:00 sedispatch
    802 ?        00:00:00 ModemManager
    806 ?        00:00:00 firewalld -> pare feu secondaire de fedora
    808 ?        00:00:00 irqbalance -> 
    809 ?        00:00:00 mcelog
    810 ?        00:00:11 rngd
    811 ?        00:00:00 rsyslogd
    812 ?        00:00:00 smartd
    813 ?        00:00:00 sssd
    814 ?        00:00:00 abrtd
    817 ?        00:00:00 chronyd
    825 ?        00:00:00 gssproxy
    836 ?        00:00:00 dbus-broker-lau
    838 ?        00:00:00 dbus-broker
    843 ?        00:00:00 abrt-dump-journ
    844 ?        00:00:00 abrt-dump-journ
    845 ?        00:00:00 abrt-dump-journ
    852 ?        00:00:00 polkitd -> gère les accès de groupe etc..
    854 ?        00:00:00 sssd_be
    863 ?        00:00:00 sssd_nss
    867 ?        00:00:00 systemd-logind
    877 ?        00:00:00 NetworkManager -> manager de la configuration réseau
    900 ?        00:00:00 sshd -> service ssh pour les connexion sécuriséé
    903 ?        00:00:00 atd
    907 tty1     00:00:00 agetty
    991 ?        00:00:00 sshd
    994 ?        00:00:00 systemd
    995 ?        00:00:00 (sd-pam)
   1000 ?        00:00:00 sshd
   1001 pts/0    00:00:00 bash -> shell bash
   1139 pts/0    00:00:00 ps 
```

   
   
   
   2. Gestion du temps

```
 timedatectl
               Local time: Fri 2019-11-29 14:34:59 CET
           Universal time: Fri 2019-11-29 13:34:59 UTC
                 RTC time: Fri 2019-11-29 13:34:58
                Time zone: Europe/Paris (CET, +0100)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
```


**🌞 déterminer la différence entre Local Time, Universal Time et RTC time**


UTC se base sur les zone GMT (le centre de toutes les zone GMT)
RTC = Real Time Clock (le temps réel)
Local time se base sur le temp en france

**expliquer dans quels cas il peut être pertinent d'utiliser le RTC time**

RTC est la pile temps qui permet de garder le même temps même si l'ordinateur 
est éteint ou déconnecté du réseaux.


timedatectl list-timezones et timedatectl set-timezone <TZ> pour définir de 
timezones

**🌞 changer de timezone pour un autre fuseau horaire européen**

```
 timedatectl set-timezone Europe/Stockholm
==== AUTHENTICATING FOR org.freedesktop.timedate1.set-timezone ====
Authentication is required to set the system timezone.
Authenticating as: root
Password:
==== AUTHENTICATION COMPLETE ====

 timedatectl
               Local time: Fri 2019-11-29 14:43:11 CET
           Universal time: Fri 2019-11-29 13:43:11 UTC
                 RTC time: Fri 2019-11-29 13:43:10
                *Time zone: Europe/Stockholm (CET, +0100)*
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
```



on peut activer ou désactiver l'utilisation de la syncrhonisation NTP avec 
timedatectl set-ntp <BOOLEAN>


****🌞 désactiver le service lié à la synchronisation du temps avec cette 
commande, et vérifier à la main qu'il a été coupé****



```
timedatectl set-ntp 0
==== AUTHENTICATING FOR org.freedesktop.timedate1.set-ntp ====
Authentication is required to control whether network time synchronization shall
be enabled.
Authenticating as: root
Password:
==== AUTHENTICATION COMPLETE ====

 timedatectl
               Local time: Fri 2019-11-29 14:45:10 CET
           Universal time: Fri 2019-11-29 13:45:10 UTC
                 RTC time: Fri 2019-11-29 13:45:10
                Time zone: Europe/Stockholm (CET, +0100)
System clock synchronized: yes
**              NTP service: inactive**
          RTC in local TZ: no
```

          
          
          
          
3. Gestion de noms

****🌞 expliquer la différence entre les trois types de noms. Lequel est à 
utiliser pour des machines de prod ?****



```
[user@fedora ~]$ **hostnamectl set-hostname "Stephen's notebook" --pretty**
==== AUTHENTICATING FOR org.freedesktop.hostname1.set-static-hostname ====
Authentication is required to set the statically configured local host name,
as well as the pretty host name.
Authenticating as: root
Password:
==== AUTHENTICATION COMPLETE ====

[user@fedora ~]$ hostnamectl
   Static hostname: fedora
**   Pretty hostname: Stephen's notebook**
         Icon name: computer-vm
```

       
       
--pretty permet d'avoir un nom de machine lisible pour l'humain et non la
machine qui elle lira le nom de base.


En conclusion:

L'hostname  «transient» est un nom d’hôte dynamique géré par le kernel. Il est 
initialisé par défaut sur le hostname static, dont la valeur par défaut est
«localhost».
Il peut être modifié par DHCP ou mDNS lors de l'exécution. 
L'hostname --pretty est un nom d'hôte UTF8 de forme libre destiné à être 
présenté à l'utilisateur.


**on peut aussi modifier certains autre noms comme le type de déploiement :**

```
hostnamectl set-deployment coucou
==== AUTHENTICATING FOR org.freedesktop.hostname1.set-machine-info ====
Authentication is required to set local machine information.
Authenticating as: root
Password:
==== AUTHENTICATION COMPLETE ====
```




4. Gestion du réseau (et résolution de noms)

****🌞 afficher les informations DHCP récupérées par NetworkManager (sur une 
interface en DHCP)****


pas fait cette partie car NE JAMAIS UTILISER NETWORKMANAGER



**systemd-networkd**

**🌞 stopper et désactiver le démarrage de NetworkManager**

```
[user@fedora ~]$ systemctl stop NetworkManager
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to stop 'NetworkManager.service'.
Authenticating as: root
Password:
==== AUTHENTICATION COMPLETE ====

[user@fedora ~]$ systemctl is-enabled NetworkManager
disabled
[user@fedora ~]$ systemctl is-active NetworkManager
inactive
```



**🌞 démarrer et activer le démarrage de systemd-networkd**

```
systemctl enable systemd-networkd
Created symlink /etc/systemd/system/dbus-org.freedesktop.network1.service 
→ /usr/lib/systemd/system/systemd-networkd.service.
Created symlink /etc/systemd/system/sockets.target.wants/systemd-networkd.socket 
→ /usr/lib/systemd/system/systemd-networkd.socket.

 systemctl status systemd-networkd
● systemd-networkd.service - Network Service
   Loaded: loaded (/usr/lib/systemd/system/systemd-networkd.service; enabled;
   Active: active (running) since Fri 2019-11-29 16:35:13 CET; 2min 9s ago
     Docs: man:systemd-networkd.service(8)
 Main PID: 1600 (systemd-network)
   Status: "Processing requests..."
    Tasks: 1 (limit: 9509)
   Memory: 2.1M
   CGroup: /system.slice/systemd-networkd.service
           └─1600 /usr/lib/systemd/systemd-networkd
```


🌞 éditer la configuration d'une carte réseau de la VM avec un fichier .network


```
 cd /etc/systemd/network/
[root@fedora network]# ls
conf1.network
```



 ```
nano /etc/systemd/network/conf1.network
[Match]
Key=enp0s3

[Network]
DHCP=yes

[DHCPv4]
UseDNS=true
```

**systemd-resolved**

🌞 activer la résolution de noms par systemd-resolved en démarrant le service 
(maintenant et au boot)


 ```
systemctl start systemd-resolved.service
 
[root@fedora network]# systemctl enable systemd-resolved.service
Created symlink /etc/systemd/system/dbus-org.freedesktop.resolve1.service 
→ /usr/lib/systemd/system/systemd-resolved.service.

 systemctl status systemd-resolved.service
● systemd-resolved.service - Network Name Resolution
   Loaded: loaded (/usr/lib/systemd/system/systemd-resolved.service; enabled;
   Active: active (running) since Fri 2019-11-29 22:44:55 CET; 1min 2s ago
  
 
```

🌞 vérifier qu'un serveur DNS tourne localement et écoute sur un port de 
l'interfce localhost (avec ss par exemple)

ss prend le relais netstat qui devient obsolete sous unix

ss -ltunp (-l -> affiche que les ecoute, -t affiche tcp, -u affiche udp, 
-n -> ne resout pas les nom (pour ne pas fficher
domaine, -p pour afficher les ^processus associé au port ecoute )

```
ss -ltunp
Netid State  Recv-Q Send-Q       Local Address:Port   Peer Address:Port
**udp   UNCONN 0      0            127.0.0.53%lo:53          0.0.0.0:*     users:(("systemd-resolve",pid=914,fd=17))**
udp   UNCONN 0      0      192.168.0.33%enp0s3:68          0.0.0.0:*     users:(("systemd-network",pid=876,fd=20))
udp   UNCONN 0      0                  0.0.0.0:5355        0.0.0.0:*     users:(("systemd-resolve",pid=914,fd=12))
udp   UNCONN 0      0                     [::]:5355           [::]:*     users:(("systemd-resolve",pid=914,fd=14))
**tcp   LISTEN 0      128          127.0.0.53%lo:53          0.0.0.0:*     users:(("systemd-resolve",pid=914,fd=18))**
tcp   LISTEN 0      128                0.0.0.0:22          0.0.0.0:*     users:(("sshd",pid=942,fd=5))
tcp   LISTEN 0      128                0.0.0.0:5355        0.0.0.0:*     users:(("systemd-resolve",pid=914,fd=13))
tcp   LISTEN 0      128                   [::]:22             [::]:*     users:(("sshd",pid=942,fd=7))
tcp   LISTEN 0      128                      *:9090              *:*     users:(("systemd",pid=1,fd=51))
tcp   LISTEN 0      128                   [::]:5355           [::]:*     users:(("systemd-resolve",pid=914,fd=15))
```

**effectuer une commande de résolution de nom en utilisant explicitement le serveur DNS mis en place par systemd-resolved (avec dig)**

 ```
dig 127.0.0.53

; <<>> DiG 9.11.13-RedHat-9.11.13-2.fc31 <<>> 127.0.0.53
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 45723
;; flags: qr rd ra; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4000
;; QUESTION SECTION:
;127.0.0.53.                    IN      A

;; AUTHORITY SECTION:
.                       10800   IN      SOA     a.root-servers.net. 
nstld.verisign-grs.com. 2019120500 1800 900 604800 86400

;; Query time: 18 msec
;; SERVER: 89.2.0.1#53(89.2.0.1)
;; WHEN: Thu Dec 05 14:38:05 CET 2019
;; MSG SIZE  rcvd: 114
```


**effectuer une requête DNS avec systemd-resolve**

```
systemd-resolve 8.8.8.8
8.8.8.8: resolve call failed: '8.8.8.8.in-addr.arpa' not found
[root@fedora ~]# systemd-resolve google.fr
google.fr: 216.58.215.35                       -- link: enp0s3

-- Information acquired via protocol DNS in 10.9ms.
-- Data is authenticated: no
```


🌞 Afin d'activer de façon permanente ce serveur DNS, la bonne pratique est de
remplacer /etc/resolv.conf par un lien symbolique pointant vers
/run/systemd/resolve/stub-resolv.conf





```
[root@fedora ~]# ln -sf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf

 ls -l /etc/resolv.conf
lrwxrwxrwx 1 root root 37 Dec  5 14:46 /etc/resolv.conf ->
/run/systemd/resolve/stub-resolv.conf

```


**🌞 Modifier la configuration de systemd-resolved**

```
resolvectl
Global
       LLMNR setting: yes
MulticastDNS setting: yes
  DNSOverTLS setting: no
      DNSSEC setting: allow-downgrade
    DNSSEC supported: yes
         DNS Servers: 208.67.222.222
                      208.67.220.220
Fallback DNS Servers: 1.1.1.1
                      8.8.8.8
                      1.0.0.1
                      8.8.4.4
```

**🌞 mise en place de DNS over TLS**

avantage -> meilleure sécurité de résolution de noms, empeche les ecoute de
requete DNS sur une attaque de type man in the middle


**effectuer une configuration globale (dans /etc/systemd/resolved.conf)**

```
[root@fedora ~]# resolvectl query facebook.com
facebook.com: 31.13.92.36                      -- link: enp0s3

-- Information acquired via protocol DNS in 18.9503s.
-- Data is authenticated: no
```


**prouver avec tcpdump que les résolutions sont bien à travers TLS (les serveurs
DNS qui supportent le DNS over TLS écoutent sur le port 853)**




fonctionne avec les serveur CloudFlare (support dnsOverTLS)

```
Frame 5: 611 bytes on wire (4888 bits), 611 bytes captured (4888 bits)
Ethernet II, Src: PcsCompu_1b:c1:d9 (08:00:27:1b:c1:d9), Dst: Sagemcom_5c:d2:74 (f4:6b:ef:5c:d2:74)
Internet Protocol Version 4, Src: 192.168.0.33, Dst: 1.1.1.1
Transmission Control Protocol, Src Port: 58066, Dst Port: 853, Seq: 1, Ack: 1, Len: 557
Transport Layer Security
    TLSv1.2 Record Layer: Handshake Protocol: Client Hello
        Content Type: Handshake (22)
        Version: TLS 1.2 (0x0303)
        Length: 552
        Handshake Protocol: Client Hello
```





```
Frame 10: 178 bytes on wire (1424 bits), 178 bytes captured (1424 bits)
Ethernet II, Src: PcsCompu_1b:c1:d9 (08:00:27:1b:c1:d9), Dst: Sagemcom_5c:d2:74 (f4:6b:ef:5c:d2:74)
Internet Protocol Version 4, Src: 192.168.0.33, Dst: 1.1.1.1
Transmission Control Protocol, Src Port: 58066, Dst Port: 853, Seq: 609, Ack: 148, Len: 124
Transport Layer Security
    TLSv1.2 Record Layer: Application Data Protocol: dns
        Content Type: Application Data (23)
        Version: TLS 1.2 (0x0303)
        Length: 26
        Encrypted Application Data: 0000000000000001d3733ea1bec2e7c28b3eb3f6827d219a…
    TLSv1.2 Record Layer: Application Data Protocol: dns
        Content Type: Application Data (23)
        Version: TLS 1.2 (0x0303)
        Length: 88
        Encrypted Application Data: 00000000000000028e701eac3bb7ff781fb1d1941559291e…
```


**🌞 activer l'utilisation de DNSSEC**


```
 systemd-resolve --status | grep DNSSEC
 
      DNSSEC setting: yes
    DNSSEC supported: yes
          DNSSEC NTA: 10.in-addr.arpa
      DNSSEC setting: yes
    DNSSEC supported: yes
```


```
dig www.dnssec-failed.org | grep status

 dig www.estcequecestbientotlapero.fr | grep status
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 11145
```





5. Gestion de sessions logind


6. Gestion d'unité basique (services)


**🌞 trouver l'unité associée au processus chronyd**


 ```
[root@fedora ~]# ps -ef | grep chronyd
chrony     10953       1  0 10:47 ?        00:00:00 chronyd

 systemctl status 10953
● session-3.scope - Session 3 of user root
   Loaded: loaded (/run/systemd/transient/session-3.scope; transient)
Transient: yes
   Active: active (running) since Thu 2019-12-05 12:44:54 CET; 22h ago
    Tasks: 6
   Memory: 18.4M
   CGroup: /user.slice/user-0.slice/session-3.scope
           ├─ 1023 sshd: root [priv]
           ├─ 1025 sshd: root@pts/0
           ├─ 1026 -bash
           ├─10953 chronyd
           ├─10961 systemctl status 10953
           └─10962 less


```

II. Boot et Logs

**🌞 générer un graphe de la séquence de boot**

systemd-analyze plot > graphe.svg

sshd.service (128ms)


III. Mécanismes manipulés par systemd


1. cgroups



**🌞 identifier le cgroup utilisé par votre session SSH**


```
[root@fedora ~]# ps -e -o pid,cmd,cgroup | grep sshd

    945 /usr/sbin/sshd -D -oCiphers 10:devices:/system.slice/sshd.service,9:memory:/system.slice/sshd.service,2:pids:/system.slice/sshd.service,1:name=systemd:/system.slice/sshd.service,0::/system.slice/sshd.service
    963 sshd: root [priv]           10:devices:/user.slice,9:memory:/user.slice/user-0.slice/session-1.scope,2:pids:/user.slice/user-0.slice/session-1.scope,1:name=systemd:/user.slice/user-0.slice/session-1.scope,0::/user.slice/user-0.slice/session-1.scope
    972 sshd: root@pts/0            10:devices:/user.slice,9:memory:/user.slice/user-0.slice/session-1.scope,2:pids:/user.slice/user-0.slice/session-1.scope,1:name=systemd:/user.slice/user-0.slice/session-1.scope,0::/user.slice/user-0.slice/session-1.scope
   1014 sshd: root [priv]           10:devices:/user.slice,9:memory:/user.slice/user-0.slice/session-3.scope,2:pids:/user.slice/user-0.slice/session-3.scope,1:name=systemd:/user.slice/user-0.slice/session-3.scope,0::/user.slice/user-0.slice/session-3.scope
   1016 sshd: root@notty            10:devices:/user.slice,9:memory:/user.slice/user-0.slice/session-3.scope,2:pids:/user.slice/user-0.slice/session-3.scope,1:name=systemd:/user.slice/user-0.slice/session-3.scope,0::/user.slice/user-0.slice/session-3.scope
   1125 grep --color=auto sshd      10:devices:/user.slice,9:memory:/user.slice/user-0.slice/session-1.scope,2:pids:/user.slice/user-0.slice/session-1.scope,1:name=systemd:/user.slice/user-0.slice/session-1.scope,0::/user.slice/user-0.slice/session-1.scope

```

identifier la RAM maximale à votre disposition (dans /sys/fs/cgroup)

memory.limit_in_bytes		 # set/show limit of memory usage

```
[root@fedora ~]# cat /sys/fs/cgroup/memory/memory.limit_in_bytes
9223372036854771712
```


**🌞 modifier la RAM dédiée à votre session utilisateur**

user-0.slice = user root

` [root@fedora ~]# systemctl set-property user-0.slice MemoryMax=512M`

**🌞 vérifier la création du fichier**

lorsque l'on créé une limite de mémoire RAM, cela créé un fichier dans
/etc/systemd/system.control

```
[root@fedora ~]# cat /etc/systemd/system.control/user-0.slice.d/50-MemoryMax.conf
# This is a drop-in unit file extension, created via "systemctl set-property"
# or an equivalent operation. Do not edit.
[Slice]
MemoryMax=536870912
[root@fedora ~]#
```

2. D-Bus


partie non effectuée

3. Restriction et isolation


**🌞 identifier le cgroup utilisé**


```
 sudo systemd-run --wait -t /bin/bash
Running as unit: run-u25.service
Press ^] three times within 1s to disconnect TTY.
[root@fedora /]# systemctl status run-u25.service
● run-u25.service - /bin/bash
   Loaded: loaded (/run/systemd/transient/run-u25.service; transient)
Transient: yes
   Active: active (running) since Sun 2019-12-08 17:48:39 CET; 25s ago
 Main PID: 1642 (bash)
    Tasks: 3 (limit: 9509)
   Memory: 6.3M
   CGroup: /system.slice/run-u25.service
           ├─1642 /bin/bash
           ├─1693 systemctl status run-u25.service
           └─1694 less
```

```
systemd-cgls

system.slice
├─run-u25.service
  │ ├─1642 /bin/bash
  │ ├─1699 systemd-cgls
  │ └─1700 less
```


le cgroups est system.slice

```
ls /sys/fs/cgroup/systemd/system.slice/run-u25.service/
cgroup.clone_children  cgroup.procs           notify_on_release      tasks
```

les restriction appliquées sont:

**🌞 lancer une nouvelle commande : ajouter des restrictions cgroups**

```
sudo systemd-run -p MemoryMax=512M --wait -t /bin/bash
Running as unit: run-u33.service
Press ^] three times within 1s to disconnect TTY.
```

**🌞 lancer une nouvelle commande : ajouter un traçage réseau**

```
sudo systemd-run -p IPAccounting=true --wait -t /bin/bash
Running as unit: run-u37.service
Press ^] three times within 1s to disconnect TTY.
```

**🌞 lancer une nouvelle commande : ajouter des restrictions réseau**

**prouver que les restrictions réseau sont effectives**

pas compris cette partie














































